﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for EmailService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class EmailService : System.Web.Services.WebService
{

    public EmailService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public bool SendEmail(string body, string subject, string toAddress)
    {
        bool sended = false;
        string[] emailList;

        try
        {
            emailList = new string[toAddress.Split(';').Length];
            emailList = toAddress.Split(';');

            using (var message = new MailMessage())
            {
                foreach (string addr in emailList)
                {
                    message.To.Add(new MailAddress(addr));
                }
                //message.To.Add(new MailAddress(toAddress));
                message.From = new MailAddress("Servicerequest@pamc.co.za", "Portal notification");
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = false;

                using (var client = new SmtpClient("192.168.16.1"))
                {
                    client.Host = "192.168.16.1";
                    client.UseDefaultCredentials = false;
                    client.Port = 465;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Credentials = new NetworkCredential("Servicerequest", "12345678");
                    ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                    client.EnableSsl = true;
                    client.Send(message);
                    sended = true;
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.Message;
            return sended;
        }

        return sended;
    }

}
