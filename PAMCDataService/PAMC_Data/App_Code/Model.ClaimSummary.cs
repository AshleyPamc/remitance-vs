﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class ClaimSvcDateSummary
{
    public int SVCYear;
    public int SVCMonth;
    public string HPCode;
    public string Option;
    public decimal Net;
}