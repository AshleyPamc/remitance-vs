﻿using ReportingTableAdapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ReportData : System.Web.Services.WebService
{
    SqlConnection _cn = null;
    public ReportData()
    {
        string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DRCConnectioString"].ConnectionString;
        this._cn = new SqlConnection(connStr);
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public List<ClaimSvcDateSummary> GetClaimsData()
    {
        //ClaimsReportTableAdapter ta = new ClaimsReportTableAdapter();
        DWTableAdapters.ServiceDateClaimsTableAdapter ta = new DWTableAdapters.ServiceDateClaimsTableAdapter();
        List<ClaimSvcDateSummary> dataList = new List<ClaimSvcDateSummary>();

        //Reporting d = new Reporting();

        DW f = new DW();
        try
        {
            if (this._cn.State != System.Data.ConnectionState.Open)
            {
                this._cn.Open();
            }
            ta.Connection = this._cn;
            ta.FillByRiskHPCODE(f.ServiceDateClaims, "Gets");
            foreach (var item in f.ServiceDateClaims)
            {
                ClaimSvcDateSummary row = new ClaimSvcDateSummary();
                row.SVCYear = item.SVCYear;
                row.SVCMonth = item.SVCMonth;
                row.Net = item.Net;
                dataList.Add(row);
            }
        }
        catch (Exception ex)
        {

            throw;
        }
        return dataList;
    }
}
