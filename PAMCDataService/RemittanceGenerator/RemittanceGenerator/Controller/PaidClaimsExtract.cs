﻿using RemittanceGenerator.Formats;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Data.SqlClient;

namespace RemittanceGenerator.Controller
{
    public class PaidClaimsExtract
    {
        Tables.RemittanceDataSet dsERA = new Tables.RemittanceDataSet();
        //Tables.RemittanceDataSetTableAdapters.RemittanceDataTableAdapter taEra = new Tables.RemittanceDataSetTableAdapters.RemittanceDataTableAdapter();
        private Tables.RemittanceDataSetTableAdapters.RemittanceDataTableAdapter taERA = new Tables.RemittanceDataSetTableAdapters.RemittanceDataTableAdapter();
        //private eRaExport dsERA = new eRaExport();
        private GalaxyNet.EZCAP.Tables.Claims dsClm = new GalaxyNet.EZCAP.Tables.Claims();
        private GalaxyNet.EZCAP.Tables.ClaimsTableAdapters.CLAIM_ADJUSTSTableAdapter taClmAd = new GalaxyNet.EZCAP.Tables.ClaimsTableAdapters.CLAIM_ADJUSTSTableAdapter();
        private GalaxyNet.EZCAP.Tables.ClaimsTableAdapters.ADJUST_CODESTableAdapter taAdjCd = new GalaxyNet.EZCAP.Tables.ClaimsTableAdapters.ADJUST_CODESTableAdapter();
        private string _txtERAPath = "";
        private Int32 _buroID = 0;
        private string _provId = "";
        private DateTime _datePaid = new DateTime();
        private string _claimno = "";
        private bool _memProv = false;

        private bool _file = false;

        #region IFileToExport Members
        System.Collections.Generic.List<FileInfo> _lResultFileList = new System.Collections.Generic.List<FileInfo>();
        public List<FileInfo> ResultFileList
        {
            get
            {
                return _lResultFileList;
            }
            set
            {
                _lResultFileList = value;
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputPath">Directory of where the output file needs to be saved</param>
        /// <param name="buroID">Buro ID</param>
        /// <param name="datePaid">Date of Payment</param>
        /// <param name="ProvID">Provider ID</param>
        /// <param name="file">Boolean value TRUE=Text & FALSE=Pdf</param>
        /// 
        public PaidClaimsExtract(string outputPath,Int32 buroID, DateTime datePaid, string ProvID, bool file)
        {
            _datePaid = datePaid;
            _buroID = buroID;
            _txtERAPath = outputPath;
            _provId = ProvID;
            _file = file;
        }

        public PaidClaimsExtract(string outputPath, DateTime datePaid, string ProvID, bool file)
        {
            _datePaid = datePaid;
            _txtERAPath = outputPath;
            _provId = ProvID;
            _file = file;
        }

        public PaidClaimsExtract(string outputPath,  string Claimno, bool file)
        {
            _claimno = Claimno;
            _txtERAPath = outputPath;
            _file = file;
        }

        public PaidClaimsExtract(string outputPath, string Claimno, bool memProv, bool file)
        {
            _claimno = Claimno;
            _txtERAPath = outputPath;
            _memProv = memProv;
            _file = file;
        }

        public void StartProcessing()
        {
            ProdueceInterpharmeRa();
        }

        private void ProdueceInterpharmeRa()
        {
            ArrayList _arr = new ArrayList();
            eRaSet era = new eRaSet();
            // taERA.SetTimeOut(600);
            if (_claimno != "")
            {
                try
                {
                    taERA.FillByClaimno(dsERA.RemittanceData, _claimno);
                }
                catch (Exception x)
                {
                    Exception ex = new Exception($"An Error Occured: {taERA.Connection.ConnectionString}",x);
                  
                    throw ex;
                }
            }
            else if (_buroID != 0)
            {
                //Int32 cntUpdate = taERA.UpdateQuery(_buroEmail, _datePaid);

                //if (cntUpdate > 0)
                //{
                try
                {
                    taERA.Fill(dsERA.RemittanceData, _buroID, _datePaid);
                }
                catch (Exception ex)
                {
                    if (System.Environment.UserInteractive)
                    {
                        //MessageBox.Show("An error occurred : \r\n" + ex.Message + "\r\n" + ex.StackTrace);
                    }
                }
                //}
            }
            else
            {

                taERA.FillByVendorDatepaid(dsERA.RemittanceData, _provId, _datePaid);
            }
            if (_file)
            {
               
                RetriveFiles(_arr, era);
            }
            else
            {
                
                GenerateRemittances(_arr, era, "Data Source=192.168.16.13;Initial Catalog=DRC;User ID=PamcPortal;Password=#Portal@Pamc", "Data Source=192.168.16.13;Initial Catalog=Reporting;User ID=PamcPortal;Password=#Portal@Pamc");
            }
        }

        private void RetriveFiles(ArrayList _arr, eRaSet era)
        {
            if (dsERA.RemittanceData.Count > 0)
            {

                eRaFileHeader h = new eRaFileHeader();

                h.BATCH = DateTime.Now.ToString("yyyyMMdd");

                h.CTEATEDATE = DateTime.Now.ToString("yyyyMMdd");
                h.SWITCH = _buroID.ToString();
                _arr.Add(h);
                Decimal dFileTotal = 0;

                string prevVENDOR = "";
                string prevDatePaid = "";
                Decimal dProvTotal = 0;
                Int64 cntProv = 0;

                foreach (Tables.RemittanceDataSet.RemittanceDataRow rERA in dsERA.RemittanceData)
                {
                    try
                    {

                        if (rERA.VENDOR != prevVENDOR | rERA.DATEPAID.ToString("yyyyMMdd") != prevDatePaid)
                        {
                            if (prevVENDOR != "")
                            {
                                eRaProviderFooter epf = new eRaProviderFooter();
                                epf.PROVIDERLINES = cntProv.ToString();
                                epf.PROVIDERTOTAL = dProvTotal.ToString("f");
                                _arr.Add(epf);
                                dProvTotal = 0;
                                cntProv = 0;
                            }
                            eRaProviderHeader ph = new eRaProviderHeader();
                            //ph.ACCTNO = rERA.ACCNR;
                            //ph.ACCTTYPE = rERA.ACCTYPE;
                            //ph.BANKNAME = rERA.BANK;
                            //ph.BRANCHNUMBER = rERA.BRANCHCODE;
                            ph.DATEPAID = rERA.DATEPAID.ToString("yyyyMMdd");
                            ph.PROVID = rERA.VENDOR;

                            _arr.Add(ph);
                        }


                        try
                        {
                            eRaClaimDetail d = new eRaClaimDetail();
                            if (rERA.ADJCODEWH.Trim().Length > 0)
                            {
                                d.ADJCODE = rERA.ADJCODEWH.Trim();
                                d.ADJUST = rERA.ADJUSTWH.ToString("f");
                                d.ADJDESC = rERA.WHDESC;
                            }
                            else
                            {
                                d.ADJCODE = rERA.ADJCODE.Trim();
                                d.ADJUST = rERA.ADJUST.ToString("f");
                                d.ADJDESC = rERA.ADJDESC;
                            }
                            if (d.ADJCODE.Trim() == "##")
                            {
                                taClmAd.Fill(dsClm.CLAIM_ADJUSTS, rERA.CLAIMNO, rERA.TBLROWID);
                                int cntAdj = 0;
                                foreach (GalaxyNet.EZCAP.Tables.Claims.CLAIM_ADJUSTSRow rCA in dsClm.CLAIM_ADJUSTS.Rows)
                                {
                                    cntAdj++;
                                    taAdjCd.Fill(dsClm.ADJUST_CODES, rCA.ADJCODE);
                                    if (cntAdj == 1)
                                    {
                                        d.ADJCODE = rCA.ADJCODE.Trim();
                                        d.ADJDESC = dsClm.ADJUST_CODES[0].DESCR.Trim();
                                    }
                                    else
                                    {
                                        if (!d.ADJCODE.Contains(rCA.ADJCODE.Trim()))
                                        {
                                            d.ADJCODE = d.ADJCODE + ';' + rCA.ADJCODE.Trim();
                                            d.ADJDESC = d.ADJDESC + ';' + dsClm.ADJUST_CODES[0].DESCR.Trim();
                                        }

                                    }

                                }

                            }
                            d.BILLED = rERA.BILLED.ToString("f");
                            d.CLAIMNO = rERA.CLAIMNO;
                            d.CONTRACTVALUE = rERA.SCHEMETARRIF.ToString("f");
                            d.COPAY = rERA.COPAY.ToString("f");
                            d.DIAGCODE = rERA.DIAGCODE;
                            d.FROMDATESVC = rERA.FROMDATESVC.ToString("yyyyMMdd");
                            d.MEMBERBIRTH = rERA.BIRTH.ToString("yyyyMMdd");

                            d.MEMBID = rERA.MEMBID;
                            //if (_buroEmail == "RECON")
                            //{
                            //    d.MEMBNAME = rERA.HPCODE;
                            //}

                            d.NAPPI = rERA.PROCCODE.Trim();
                            d.NET = rERA.NET.ToString("f");

                            if (rERA.PROVCLAIM.Contains("/"))
                            {
                                d.PROVCLAIM = rERA.PROVCLAIM.Substring(rERA.PROVCLAIM.IndexOf('/') + 1);

                                if (d.PROVCLAIM.Contains("/"))
                                {
                                    d.PROVCLAIM = d.PROVCLAIM.Substring(0, d.PROVCLAIM.LastIndexOf('/'));
                                    d.IPBATCH = d.PROVCLAIM.Substring(d.PROVCLAIM.LastIndexOf('/') + 1);
                                }
                                else
                                {
                                    d.PROVCLAIM = rERA.PROVCLAIM;
                                }
                            }
                            else
                            {
                                d.PROVCLAIM = rERA.PROVCLAIM;
                                //if (!rERA.IsBATCH_NONull())
                                //{
                                //    d.IPBATCH = rERA.BATCH_NO;
                                //}
                                if (!rERA.IsCHPREFIXNull())
                                {
                                    d.IPBATCH = rERA.CHPREFIX.ToString();
                                }
                            }

                            dProvTotal += rERA.NET;
                            dFileTotal += rERA.NET;
                            cntProv++;
                            _arr.Add(d);

                            prevVENDOR = rERA.VENDOR;
                            prevDatePaid = rERA.DATEPAID.ToString("yyyyMMdd");

                        }
                        catch (Exception ex)
                        {
                            if (System.Environment.UserInteractive)
                            {
                                // MessageBox.Show("An error occurred : \r\n" + ex.Message + "\r\n" + ex.StackTrace);
                            }
                            //ClsEventLog._eLog.WriteToEventLog("E", "PaidClaimsExport", "An error occurred: \r\nMessage: " + ex.Message + "\r\nStackTrace: " + ex.StackTrace);
                        }

                    }
                    catch (Exception ex)
                    {
                        if (System.Environment.UserInteractive)
                        {
                            //MessageBox.Show("An error occurred : \r\n" + ex.Message + "\r\n" + ex.StackTrace);
                        }
                        //ClsEventLog._eLog.WriteToEventLog("E", "PaidClaimsExport", "An error occurred: \r\nMessage: " + ex.Message + "\r\nStackTrace: " + ex.StackTrace);
                    }
                }
                eRaProviderFooter pf = new eRaProviderFooter();
                pf.PROVIDERLINES = cntProv.ToString();
                pf.PROVIDERTOTAL = dProvTotal.ToString("f");
                _arr.Add(pf);
                eRaFileFooter f = new eRaFileFooter();
                f.COUNT = dsERA.RemittanceData.Count.ToString();
                f.FILETOTALNET = dFileTotal.ToString("f");
                _arr.Add(f);

                string fileName = _txtERAPath + "\\PaidClaims_" + _provId + "_" + _datePaid.ToString("yyyyMMdd") + "_Era.txt";

                era.WriteeRaSet(fileName, _arr.ToArray());
                ResultFileList.Add(new FileInfo(fileName));
                if (System.Environment.UserInteractive)
                {
                    //MessageBox.Show("Done!");
                }
                //ClsEventLog._eLog.WriteToEventLog("S", "PaidClaimsExport", "File exported successfully!");
            }
            else
            {
                if (System.Environment.UserInteractive)
                {
                    //ClsEventLog._eLog.WriteToEventLog("W", "PaidClaimsExport", "No Paid Claims for the past month!");
                }
            }
        }

        private void GenerateRemittances(ArrayList _arr, eRaSet era, string strConnectstring, string strRepConnString)
        {
            if (dsERA.RemittanceData.Count > 0)
            {


                Decimal dFileTotal = 0;

                string prevVendID = "";
                string prevDatePaid = "";
                Decimal dProvTotal = 0;
                Int64 cntProv = 0;


                SqlConnection cnRep = new SqlConnection(strRepConnString);
                cnRep.Open();

                GalaxyNet.EZCAP.Controllers.EzcapRemittanceGenerater cERG = new GalaxyNet.EZCAP.Controllers.EzcapRemittanceGenerater(strConnectstring, strRepConnString);
                ArrayList al = new ArrayList();
                foreach (Tables.RemittanceDataSet.RemittanceDataRow rERA in dsERA.RemittanceData)
                {
                    try
                    {

                        if (rERA.VENDOR != prevVendID | rERA.DATEPAID.ToString("yyyyMMdd") != prevDatePaid)
                        {
                            GalaxyNet.Reporting.Tables.Remittances dsR = new GalaxyNet.Reporting.Tables.Remittances();
                            GalaxyNet.Reporting.Tables.RemittancesTableAdapters.CHECK_RUN_DOCSTableAdapter taCRD = new GalaxyNet.Reporting.Tables.RemittancesTableAdapters.CHECK_RUN_DOCSTableAdapter();
                            taCRD.Connection = new System.Data.SqlClient.SqlConnection(strRepConnString);

                            taCRD.FillByPrefixCheckno(dsR.CHECK_RUN_DOCS, rERA.CHPREFIX, rERA.CHECKNO);

                            if (dsR.CHECK_RUN_DOCS.Count > 0)
                            {
                                int deleted = GalaxyNet.Reporting.Tools.RepTools.DeleteRemittancesFromCheckRunDocs(cnRep, rERA.CHPREFIX.ToString(), rERA.CHECKNO.ToString());
                            }
                            int dummy1 = 0;
                            int dummy2 = 0;
                            string claimType = "P";
                            if (_claimno != "")
                            {
                                claimType = rERA.PAIDTO;
                            }
                            string email = "";
                            if (!rERA.IsEMAILNull())
                            {
                                email = rERA.EMAIL;
                            }
                            Byte[] arPdfFile = null;
                            if (_memProv)
                            {
                                arPdfFile = cERG.CreateCheckRunDoc(ref dummy1, ref dummy2, rERA.CHPREFIX, rERA.CHECKNO, rERA.DATEPAID, email, rERA.VENDOR, claimType, false, rERA.SUBSSN, "E", "", "");

                            }
                            else
                            {
                                arPdfFile = cERG.CreateCheckRunDoc(ref dummy1, ref dummy2, rERA.CHPREFIX, rERA.CHECKNO, rERA.DATEPAID, email, rERA.VENDOR, claimType, true, "", "E", "", "");
                            }
                           
                            //   taCRD.FillByPrefixCheckno(dsR.CHECK_RUN_DOCS, rERA.CHPREFIX, rERA.CHECKNO);
                            string nameR = "";
                            if (_claimno != "")
                            {
                                nameR = _txtERAPath + @"\Rem_" + rERA.PROVID.ToString() + "_" + rERA.MEMBID.ToString().Trim() + ".pdf";
                            }
                            else
                            {
                                nameR = _txtERAPath + @"\Rem_" + rERA.CHPREFIX.ToString() + "_" + rERA.VENDOR.ToString().Trim() + ".pdf";
                            }
                            
                            FileInfo fileRem = GalaxyNet.Reporting.Controllers.PDFRemittance.GetFile(arPdfFile, nameR);
                            al.Add(fileRem);
                            //}
                            //else
                            //{
                            //    Byte[] arPdfFile = dsR.CHECK_RUN_DOCS[0].PAYRUNDOC;
                            //    string nameR = _txtERAPath + @"\\Rem_" + rERA.CHPREFIX.ToString() + "_" + rERA.VENDOR.ToString().Trim() + ".pdf";
                            //    FileInfo fileRem = GalaxyNet.Reporting.Controllers.PDFRemittance.GetFile(arPdfFile, nameR);
                            //    al.Add(fileRem);
                            //}


                            //era.WriteeRaSet(name, _arr.ToArray());
                            //ResultFileList.Add(new FileInfo(fileName));
                        }
                        prevVendID = rERA.VENDOR;
                        prevDatePaid = rERA.DATEPAID.ToString("yyyyMMdd");



                    }
                    catch (Exception ex)
                    {
                        if (System.Environment.UserInteractive)
                        {
                            //MessageBox.Show("An error occurred : \r\n" + ex.Message + "\r\n" + ex.StackTrace);
                        }
                        //ClsEventLog._eLog.WriteToEventLog("E", "PaidClaimsExport", "An error occurred: \r\nMessage: " + ex.Message + "\r\nStackTrace: " + ex.StackTrace);
                    }
                }


                string fileName = "";
                if (_claimno != "")
                {
                    fileName = _txtERAPath + @"\Rem_" + _claimno;
                }
                else
                {
                    fileName = _txtERAPath + "\\Remits_" + _provId + "_" + _datePaid.ToString("yyyyMMdd");
                }
                
                FileInfo fileToZip = new FileInfo(fileName);
                string zippedfile = PAMC.Tools.PAMCTools.ZipFileInDirectory(fileToZip.FullName, "", true);
                //era.WriteeRaSet(fileName, _arr.ToArray());
                ResultFileList.Add(new FileInfo(zippedfile));
                if (System.Environment.UserInteractive)
                {
                    //MessageBox.Show("Done!");
                }
                //ClsEventLog._eLog.WriteToEventLog("S", "PaidClaimsExport", "File exported successfully!");
            }
            else
            {
                if (System.Environment.UserInteractive)
                {
                    //ClsEventLog._eLog.WriteToEventLog("W", "PaidClaimsExport", "No Paid Claims for the past month!");
                }
            }
        }
    }
}
